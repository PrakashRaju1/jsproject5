const cacheFunction = (cb) => {
    let cache = {};
    //console.log(cache);
    return (...args) => {
      let n = args[0];  
      if (n in cache) {
        return cache[n];
      }
      else {
        let result = cb(n);
        cache[n] = result;
        return result;
      }
    }
  }

  module.exports=cacheFunction;