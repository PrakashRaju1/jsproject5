const cb = require("./test/testLimitFunctionCallCount");
let counter=0
function limitFunctionCallCount(cb,n) {

    
    return function(a,b){
        
        if (counter<n){
            counter +=1;
            return cb(a,b)
            
            
        }else{
            return null;
        }
        
        
    }
    
        
        
    }
        
    
  
   module.exports=limitFunctionCallCount;