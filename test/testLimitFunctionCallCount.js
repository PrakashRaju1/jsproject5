const limitFunctionCallCount=require("../limitFunctionCallCount");

const add=(a,b)=>a+b;

const result=limitFunctionCallCount(add,2);
console.log(result(3,4));
console.log(result(4,5));
console.log(result(6,7));