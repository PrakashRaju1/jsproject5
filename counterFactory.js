function counterFactory() {

  let currentValue = 0;

  

  let increment = function() {

      currentValue += 1;
      console.log(currentValue)
  };

  let decrement = function() {

      currentValue -= 1;
      console.log(currentValue)
  }

  return {increment: increment,
          decrement: decrement};
}

module.exports=counterFactory;